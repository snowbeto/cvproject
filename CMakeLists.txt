cmake_minimum_required(VERSION 2.8)
project( WalkingDifferentiator )

#Set flags
SET(CMAKE_CXX_FLAGS "-std=c++0x")

set(CMAKE_BUILD_TYPE Debug)

set( EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/bin)

#set (OPENCV_LIBS_SRC /home/daniel/usr/local/lib)

#Find openCV 
find_package( OpenCV REQUIRED )


#Print some opencv status
message(STATUS "OpenCV library status:")
message(STATUS "    version: ${OpenCV_VERSION}")
message(STATUS "    libraries: ${OpenCV_LIBS}")
message(STATUS "    include path: ${OpenCV_INCLUDE_DIRS}")


INCLUDE_DIRECTORIES(
	include
	${OpenCV_INCLUDE_DIRS}
	#/home/daniel/usr/local/include
        ${Boost_INCLUDE_DIR}
)

#Create executable
add_executable( WalkingDifferentiator
				src/main.cpp
				src/video_handler.cpp
				src/video_analyzer.cpp
				src/configuration.cpp 
				src/human_detector.cpp)
#link_directories(
#${OPENCV_LIBS_SRC}
#)

#Link
target_link_libraries( WalkingDifferentiator ${OpenCV_LIBS} ${Boost_LIBRARIES})

