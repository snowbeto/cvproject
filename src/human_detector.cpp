#include <human_detector.hpp>

#include <opencv2/core/core.hpp>

/*Code based on openCV implementation*/
/*https://github.com/Itseez/opencv/blob/master/samples/cpp/dbt_face_detection.cpp*/
HumanDetectorAdapter::HumanDetectorAdapter(
		cv::Ptr<cv::CascadeClassifier> detector) :
		IDetector(), a_detector(detector) {
	CV_Assert(detector);
}

void HumanDetectorAdapter::detect(const cv::Mat &Image,
		std::vector<cv::Rect> &objects) {
	a_detector->detectMultiScale(Image, objects, scaleFactor, minNeighbours, 0,
			minObjSize, maxObjSize);
}

HumanDetectorAdapter::~HumanDetectorAdapter() {
}

