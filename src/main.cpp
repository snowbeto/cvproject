#include <video_handler.hpp> 
#include <iostream>
#include <limits.h>
#include <unistd.h>

int main(int argc, char* argv[]) {

	VideoHandler handler;
	handler.run();

	return 0;
}
