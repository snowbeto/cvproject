#include <video_handler.hpp>
#include <unistd.h>
#include <fstream>

/*VideoHandler::VideoHandler(std::string video_name) :
 config_object(video_name), vidCap(config_object.get_video_full_name()), vid_analyzer(
 config_object.get_kernel_size(), config_object.get_threshold(),
 config_object.get_bg_img_full_name(),
 config_object.get_cascade_template_full_name(),
 config_object.get_canny_edge_kernel(),
 config_object.get_canny_edge_ratio(),
 config_object.get_canny_edge_threshold()), codec(
 cv::VideoWriter::fourcc('X', 'V', 'I', 'D')), GRAB(true), RECORD(
 false), frame_counter(0), counter(
 config_object.get_video_counter()), RUN(true), y2k( { 0 }), seconds(
 0.0f), seconds_s(0.0f), current_secs(0.0f), total_s(0.0f), fps(
 0), initialyze_opt_flow(true) {
 //open camera
 //vidCap.open("./videos/00001/gallery_2km/%8d.png");
 //vidCap.SetCaptureProperty(vidCap, CV_CAP_PROP_FRAME_WIDTH, 800 );
 if (!vidCap.isOpened()) {
 std::cerr << "Could not open video capture !" << std::endl;
 return;
 }

 //Calculate fps
 time(&timer);
 y2k.tm_hour = 0;
 y2k.tm_min = 0;
 y2k.tm_sec = 0;
 y2k.tm_year = 100;
 y2k.tm_mon = 0;
 y2k.tm_mday = 1;
 seconds_s = difftime(timer, mktime(&y2k));

 }*/

VideoHandler::VideoHandler() :
		config_object(""), vidCap(config_object.get_cam_id()), vid_analyzer(
				config_object.get_kernel_size(), config_object.get_threshold(),
				config_object.get_bg_img_full_name(),
				config_object.get_cascade_template_full_name(),
				config_object.get_canny_edge_kernel(),
				config_object.get_canny_edge_ratio(),
				config_object.get_canny_edge_threshold()), codec(
				cv::VideoWriter::fourcc('X', 'V', 'I', 'D')), GRAB(true), RECORD(
				false), frame_counter(0), counter(
				config_object.get_video_counter()), RUN(true), y2k( { 0 }), seconds(
				0.0f), seconds_s(0.0f), current_secs(0.0f), total_s(0.0f), fps(
				0), initialyze_opt_flow(true), initialize_running_hists_matrix(
				true), initialize_walking_hists_matrix(true), initialize_standing_hists_matrix(
				true), can_predict(false), record_and_predict(false), rotate_frames(
				false) {

	vidCap.set(CV_CAP_PROP_FPS, config_object.get_frames_per_second());
	vidCap.set(cv::CAP_PROP_FRAME_WIDTH, config_object.get_width());
	vidCap.set(cv::CAP_PROP_FRAME_HEIGHT, config_object.get_height());
	video_buffer.reserve(5000);

	//Calculate fps
	time(&timer);
	y2k.tm_hour = 0;
	y2k.tm_min = 0;
	y2k.tm_sec = 0;
	y2k.tm_year = 100;
	y2k.tm_mon = 0;
	y2k.tm_mday = 1;
	seconds_s = difftime(timer, mktime(&y2k));

	//load training data if exits
	running_hists = cv::imread(
			config_object.get_training_output_data() + "/"
					+ config_object.get_running_hist_file_name(), 0);

	walking_hists = cv::imread(
			config_object.get_training_output_data() + "/"
					+ config_object.get_walking_hist_file_name(), 0);

	standing_hists = cv::imread(
			config_object.get_training_output_data() + "/"
					+ config_object.get_standing_hist_file_name(), 0);

	if (!running_hists.empty() && !walking_hists.empty()
			&& !standing_hists.empty()) {
		//train
		std::cout << "Training" << std::endl;
		train_svn();
		can_predict = true;
	}
}

void VideoHandler::run() {
	//menu
	int opt = 1;
	bool loop = true;
	do {
		std::cout
				<< "1.- Record Videos 2.- Train SVM 3.- Classify Videos 4.- Record and Predict 5.-Take BG Image 6.-Activate rotate video 7.- Quit"
				<< std::endl;
		std::cin >> opt;

		switch (opt) {
		case 1:
			record();
			break;
		case 2:
			train_videos();
			break;
		case 3:
			predict();
			break;
		case 4:
			record_and_predict = true;
			record();
			record_and_predict = false;
			break;
		case 5:
			take_bg_image();
			break;
		case 6:
			rotate_frames = true;
			break;
		case 7:
			loop = false;
			break;
		}
	} while (loop);

}

void VideoHandler::save_video() {
//save video
	std::cout << "Saving video ... " << std::endl;
	for (int x = 0; x < video_buffer.size(); x++) {
		//save video
		vid_writer << video_buffer[x];
	}

	std::cout << "done" << std::endl;

}

void VideoHandler::take_bg_image() {
	//vidCap.release()
	vidCap >> frame;
	//rotate main frame image 90
	rotate_frame(frame);
	cv::waitKey(2);

	cv::imwrite(config_object.get_bg_img_dir() + "test_bg.jpg", frame);
	vid_analyzer.set_bg_image(frame);
	std::cout << "BG image has been recorded and passed to the vid analyzer"
			<< std::endl;

}

void VideoHandler::gui_controllers(int &keyPressed) {
	if (keyPressed == 1048603) {
		if (RECORD) {
			std::cout
					<< "Recording in progress, press s to stop current operation then press esc"
					<< std::endl;
		} else {
			GRAB = false;
			RUN = false;
		}
	} else {
		switch (keyPressed) {
		case 1048690: //r key
			if (RECORD) {
				std::cout
						<< "Video is being recorded, pres 's' to stop current video "
						<< std::endl;
			} else {
				std::cout << "Recording.... " << std::endl;
				create_video_writter();
				RECORD = true;
			}
			break;
		case 1048691:
			//s key
			if (RECORD) {
				std::cout << "Recording stopped " << std::endl;
				RECORD = false;

				save_video();
				release_video_witter();

				if (record_and_predict) {
					std::cout << "Predicting" << std::endl;
					predict(
							config_object.get_video_dir()
									+ std::to_string(counter) + "_s.avi");
				}
				counter++;
				config_object.update_counter_file(counter);

			} else {
				std::cout
						<< "No video is being recorded, press 'r' to start recording"
						<< std::endl;
			}
			break;
		}
	}
}

void VideoHandler::create_video_writter() {
//create single video writter
	vid_writer.open(
			config_object.get_video_dir() + std::to_string(counter) + "_s.avi",
			codec, config_object.get_frames_per_second(),
			cv::Size(config_object.get_height(), config_object.get_width()));

	if (!vid_writer.isOpened()) {
		std::cout << "Output L not open" << std::endl;
		return;
	}
}

void VideoHandler::release_video_witter() {
	video_buffer.clear();
	vid_writer.release();
}

void VideoHandler::calculate_fps() {
	//Calculate the FPS the system is able to capture
	time(&timer);
	seconds = difftime(timer, mktime(&y2k));
	total_s = floor(seconds_s - seconds);

	if (fps == 0) {
		current_secs = total_s;
	}

	if (current_secs != total_s) {
		std::cout << "FPS: " << fps << std::endl; //Display the current fps
		fps = 0;
	} else {
		fps++;
	}
}

void VideoHandler::rotate_frame(cv::Mat &aFrame) {
	//rotate 90
	cv::transpose(aFrame, aFrame);
	cv::flip(aFrame, aFrame, 1);
}

void VideoHandler::record() {
	// Create a window and give it a name.
	std::string windowName1 = "Video";
	//std::string windowName4 = "Lower body";

	cv::namedWindow(windowName1);
	//cv::namedWindow (windowName4);

	// Read a video frame.
	//count the number of frames per second

	while (RUN) {

		vidCap.grab();
		vidCap.retrieve(frame);

		//rotate main frame image 90
		rotate_frame(frame);

		//record
		if (RECORD) {
			//std::cout << "FPS: " << vidCap.get(CV_CAP_PROP_FPS) << std::endl;
			video_buffer.push_back(frame);
			calculate_fps();
		}

		/*if (!RECORD) {

		 cv::Mat result, resultMOG2, blackFrame, lower_body_frame;

		 //detect lower body - NOT WORKING
		 cvtColor(frame, blackFrame, cv::COLOR_RGB2GRAY);
		 lower_body_frame = frame;
		 vid_analyzer.detect_human(lower_body_frame, blackFrame);

		 cv::imshow(windowName4, lower_body_frame);

		 }*/

		// Show the image in the window.
		cv::imshow(windowName1, frame);

		// Quit the loop when a key is pressed.
		int keyPressed = cv::waitKey(1);
		gui_controllers(keyPressed);

	}
}
void VideoHandler::train_images() {

	/*read trainning dirs*/
	std::ifstream training_dirs_file(
			config_object.get_training_dir()
					+ config_object.get_training_dirs_files());
	std::string dir, subdir, image;
	/*open subject dir*/
	while (std::getline(training_dirs_file, dir)) {
		std::ifstream training_sub_dirs_file(
				config_object.get_training_dir()
						+ config_object.get_training_subdirs_file());
		/*open subdirs (2km, 3km 4km etc)*/
		while (std::getline(training_sub_dirs_file, subdir)) {

			std::string img_path(
					config_object.get_training_dir() + "/" + dir + "/"
							+ subdir);
			std::ifstream images_list(
					img_path + "/" + config_object.get_training_img_files());
			//open all images from the current subdir
			cv::Mat prev_frame;
			int c = 0;
			while (true) {
				/*Train*/
				std::getline(images_list, image);
				std::string current_image = img_path + "/" + image;
				frame = cv::imread(current_image);

				if (frame.empty()) {
					// end of file or stream
					std::cout << "End of file" << std::endl;
					std::string hist_name =
							config_object.get_training_output_hists() + "/"
									+ dir + subdir + ".png";
					//vid_analyzer.calculate_histogram(hist_name);
					initialyze_opt_flow = true;
					break;
				}

				cv::Mat foreground, edges, blackFrame, lower_body_frame;

				//Convert to gray image
				cvtColor(frame, blackFrame, cv::COLOR_RGB2GRAY);

				//vid_analyzer.detect_human(lower_body_frame, blackFrame);

				//background substraction
				vid_analyzer.backgroundSubstraction(frame, foreground);
				//vid_analyzer.backgroundSubstractionMOG2(frame, result);

				// compute human silhouette
				vid_analyzer.detect_edges(foreground, edges);

				if (initialyze_opt_flow) {
					initialyze_opt_flow = false;
					vid_analyzer.initialize_optical_flow_matrix();
					prev_frame = edges;
				} else {
					vid_analyzer.calucate_optical_flow(prev_frame, edges);
					prev_frame = edges;
				}

			}

		}

	}

}

void VideoHandler::train_videos() {
	std::string windowName1 = "Video";
	std::string windowName2 = "Black and white";
	std::string windowName3 = "Background Subtraction";
	std::string windowName4 = "Silhouette ";

	cv::namedWindow(windowName1);
	cv::namedWindow(windowName2);
	cv::namedWindow(windowName3);
	cv::namedWindow(windowName4);

	std::ifstream training_dirs_file(
			config_object.get_training_dir()
					+ config_object.get_training_dirs_files());
	std::string dir, subdir, video;
	int counter = 0; // temporary fix to deal with different directories

	/*open subject dir*/
	while (std::getline(training_dirs_file, dir)) {
		counter++;
		std::string video_path(config_object.get_training_dir() + dir + "/");
		std::ifstream video_list(
				video_path + config_object.get_training_img_files());

		//open all videos from the current subdir
		cv::Mat prev_frame;

		while (std::getline(video_list, video)) {
			std::string current_video = video_path + "/" + video;
			std::cout << "Training with file " << video << std::endl;
			cv::VideoCapture aVidCap(current_video);
			while (true) {
				/*Train*/
				cv::Mat foreground, edges, blackFrame, small_foregroung_frame;
				aVidCap >> frame;
				// end of file or stream ?
				if (frame.empty()) {
					//Yes, compute histogram and stack it in the corresponding matrix and move to the next video
					
					std::cout << "End of file" << std::endl;
					std::string hist_name =
							config_object.get_training_output_hists() + "/"
									+ video + ".png";
					cv::Mat hist;
					vid_analyzer.calculate_histogram(hist_name, hist);

					hist = hist.t();

					if (counter == 1) {
						if (initialize_running_hists_matrix) {
							initialize_running_hists_matrix = false;
							running_hists = hist;
						} else {
							running_hists.push_back(hist);
						}
					} else if (counter == 2) {
						if (initialize_walking_hists_matrix) {
							initialize_walking_hists_matrix = false;
							walking_hists = hist;
						} else {
							walking_hists.push_back(hist);
						}

					} else {
						if (initialize_standing_hists_matrix) {
							initialize_standing_hists_matrix = false;
							standing_hists = hist;
						} else {
							standing_hists.push_back(hist);
						}

					}

					initialyze_opt_flow = true;
					break;
				}
				//No: Then process each frame of the current video 
				//Convert current frame to black and white
				cvtColor(frame, blackFrame, cv::COLOR_RGB2GRAY);

				//background subtraction
				vid_analyzer.backgroundSubstraction(frame, foreground);
				//vid_analyzer.backgroundSubstractionMOG2(small_frame, foreground);

				cv::resize(foreground, small_foregroung_frame,
						cv::Size(200, 200));

				// compute human silhouette
				vid_analyzer.detect_edges(small_foregroung_frame, edges);

				//compute Dense Optical Flow Matrix
				if (initialyze_opt_flow) {
					initialyze_opt_flow = false;
					vid_analyzer.initialize_optical_flow_matrix();
					prev_frame = edges;
				} else {
					vid_analyzer.calucate_optical_flow(prev_frame, edges);
					prev_frame = edges;
				}

				cv::imshow(windowName1, frame);
				cv::imshow(windowName2, blackFrame);
				cv::imshow(windowName3, small_foregroung_frame);
				cv::imshow(windowName4, edges);
				cv::waitKey(1);

			}
		}

	}

	//All videos have been processed. Store the computed histograms in separate files.	
	//When the systems loads, it will look for this files to train the SVN.
	cv::imwrite(
			config_object.get_training_output_data() + "/"
					+ config_object.get_running_hist_file_name(),
			running_hists);
	cv::imwrite(
			config_object.get_training_output_data() + "/"
					+ config_object.get_walking_hist_file_name(),
			walking_hists);
	cv::imwrite(
			config_object.get_training_output_data() + "/"
					+ config_object.get_standing_hist_file_name(),
			standing_hists);

	//train
	train_svn();
	can_predict = true;

}
void VideoHandler::predict() {
	std::string windowName1 = "Predict: Video";
	std::string windowName2 = "Predict: Black and white";
	std::string windowName3 = "Predict: Background Subtraction";
	std::string windowName4 = "Predict: Silhouette ";

	cv::namedWindow(windowName1);
	cv::namedWindow(windowName2);
	cv::namedWindow(windowName3);
	cv::namedWindow(windowName4);

	std::ifstream predict_dirs_file(
			config_object.get_predict_dir()
					+ config_object.get_predict_dirs_files());
	std::string dir, subdir, video;
	int counter = 0; // temporary fix to deal with different directories

	/*open subject dir*/
	while (std::getline(predict_dirs_file, dir)) {
		counter++;
		std::string video_path(config_object.get_predict_dir() + dir + "/");
		std::ifstream video_list(
				video_path + config_object.get_predict_img_files());

		//open all videos from the current subdir
		cv::Mat prev_frame;
		std::cout << "***************Current prediction file " << dir
				<< "***************" << std::endl;
		while (std::getline(video_list, video)) {
			std::string current_video = video_path + "/" + video;
			std::cout << "*********Prediction with file " << video
					<< "*********" << std::endl;
			cv::VideoCapture aVidCap(current_video);
			while (true) {
				/*Predict*/
				cv::Mat foreground, edges, blackFrame, small_foregroung_frame;
				aVidCap >> frame;
				
				// end of file or stream ?
				if (frame.empty()) {
					// YES: We can now predict based on the computed Optical Flow Histogra
					std::cout << "End of file: Predicting pattern" << std::endl;
					std::string hist_name =
							config_object.get_predict_output_hists() + "/"
									+ video + ".png";
					cv::Mat hist;
					vid_analyzer.calculate_histogram(hist_name, hist);
					hist = hist.t();

					if (svm_running_walking->predict(hist) > 0.0) {
						if (svm_walking_standing->predict(hist) > 0.0) {
							std::cout << "***Prediction: Walking" << std::endl;
						} else {
							std::cout << "*Prediction: Standing" << std::endl;
						}

					} else {
						std::cout << "******Prediction: Running" << std::endl;
					}

					initialyze_opt_flow = true;
					break;
				}

				if (rotate_frames)
					rotate_frame(frame);

				//NO: Proecces frame
				//dConver current frame to black and white
				cvtColor(frame, blackFrame, cv::COLOR_RGB2GRAY);

				//background substraction
				vid_analyzer.backgroundSubstraction(frame, foreground);
				//vid_analyzer.backgroundSubstractionMOG2(small_frame, foreground);

				cv::resize(foreground, small_foregroung_frame,
						cv::Size(200, 200));

				// compute human silhouette
				vid_analyzer.detect_edges(small_foregroung_frame, edges);

				//Compute Dense Optical Flow Matrix
				if (initialyze_opt_flow) {
					initialyze_opt_flow = false;
					vid_analyzer.initialize_optical_flow_matrix();
					prev_frame = edges;
				} else {
					vid_analyzer.calucate_optical_flow(prev_frame, edges);
					prev_frame = edges;
				}

				cv::imshow(windowName1, frame);
				cv::imshow(windowName2, blackFrame);
				cv::imshow(windowName3, small_foregroung_frame);
				cv::imshow(windowName4, edges);
				cv::waitKey(1);

			}
		}

	}
}

void VideoHandler::predict(std::string fileName) {

	std::string windowName1 = "Predict: Video";
	std::string windowName2 = "Predict: Black and white";
	std::string windowName3 = "Predict: Background Subtraction";
	std::string windowName4 = "Predict: Silhouette ";
	//std::string windowName5 = "Hist";

	cv::namedWindow(windowName1);
	cv::namedWindow(windowName2);
	cv::namedWindow(windowName3);
	cv::namedWindow(windowName4);
	//cv::namedWindow(windowName5);

	cv::Mat prev_frame;
	cv::VideoCapture aVidCap(fileName);
	while (true) {
		/*Predict*/
		cv::Mat foreground, edges, blackFrame, small_foregroung_frame;
		aVidCap >> frame;

		if (frame.empty()) {
			// end of file or stream: We can now predict based on the computed Ptical Flow Histogra
			std::cout << "End of file: Predicting pattern" << std::endl;
			std::string hist_name = config_object.get_predict_output_hists()
					+ "/" + std::to_string(counter) + "_s.png";
			cv::Mat hist;
			vid_analyzer.calculate_histogram(hist_name, hist);
			hist = hist.t();

			if (svm_running_walking->predict(hist) > 0.0) {
				if (svm_walking_standing->predict(hist) > 0.0) {
					std::cout << "***Prediction: Walking" << std::endl;
				} else {
					std::cout << "*Prediction: Standing" << std::endl;
				}

			} else {
				std::cout << "******Prediction: Running" << std::endl;
			}

			initialyze_opt_flow = true;
			break;
		}

		if (rotate_frames)
			rotate_frame(frame);

		//Conver current frame to black and white
		cvtColor(frame, blackFrame, cv::COLOR_RGB2GRAY);

		//background substraction
		vid_analyzer.backgroundSubstraction(frame, foreground);
		//vid_analyzer.backgroundSubstractionMOG2(small_frame, foreground);

		cv::resize(foreground, small_foregroung_frame, cv::Size(200, 200));

		// compute human silhouette
		vid_analyzer.detect_edges(small_foregroung_frame, edges);

		if (initialyze_opt_flow) {
			initialyze_opt_flow = false;
			vid_analyzer.initialize_optical_flow_matrix();
			prev_frame = edges;
		} else {
			vid_analyzer.calucate_optical_flow(prev_frame, edges);
			prev_frame = edges;
		}

		cv::imshow(windowName1, frame);
		cv::imshow(windowName2, blackFrame);
		cv::imshow(windowName3, small_foregroung_frame);
		cv::imshow(windowName4, edges);
		cv::waitKey(1);

	}

}

void VideoHandler::train_svn() {
	//Code based on opencv

	//create labels matrix
	int lables_size = walking_hists.rows + running_hists.rows;
	int labels[lables_size];
	for (int i = 0; i < walking_hists.rows; i++) {
		labels[i] = 1;
	}
	for (int i = walking_hists.rows; i < lables_size; i++) {
		labels[i] = -1;
	}
	cv::Mat labels_mat(lables_size, 1, CV_32S, labels);

	//training svm to based on walking or running videos
	cv::Mat data = walking_hists;
	data.push_back(running_hists);
	data.convertTo(data, CV_32FC1);
	svm_running_walking = cv::ml::SVM::create();
	svm_running_walking->setType(cv::ml::SVM::C_SVC);
	svm_running_walking->setKernel(cv::ml::SVM::LINEAR);

	svm_running_walking->train(data, cv::ml::ROW_SAMPLE, labels_mat);

	//training svm to based on walking or standing
	cv::Mat data1 = walking_hists;

	//small trick to make both matrices of the same size
	cv::Mat temp1;
	cv::Mat temp2;
	temp1 = standing_hists;
	temp1.push_back(standing_hists);
	temp2 = temp1;
	temp2.push_back(temp1);
	temp2.pop_back(6);

	data1.push_back(temp2);

	data1.convertTo(data1, CV_32FC1);
	svm_walking_standing = cv::ml::SVM::create();
	svm_walking_standing->setType(cv::ml::SVM::C_SVC);
	svm_walking_standing->setKernel(cv::ml::SVM::LINEAR);

	svm_walking_standing->train(data1, cv::ml::ROW_SAMPLE, labels_mat);

	//predict();

}
