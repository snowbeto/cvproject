#include <video_analyzer.hpp>
#include <opencv2/core.hpp>
#include <opencv2/bgsegm.hpp>
#include <iostream>

/*cascade files taken form http://alereimondo.no-ip.org/OpenCV/34*/

VideoAnalyzer::VideoAnalyzer(int kernel_size, int threshold,
		std::string bg_image_file, std::string cascade_template_file,
		int canny_edge_kernel_size, int canny_edge_ratio,
		int canny_edge_threshold) :
		kernel_size(kernel_size), threshold(threshold), bg_image_file(
				bg_image_file), pMOG2(cv::createBackgroundSubtractorMOG2()), cascade_filename(
				cascade_template_file), cascade_main_detector(
				cv::makePtr<cv::CascadeClassifier>(cascade_filename)), main_detector(
				cv::makePtr<HumanDetectorAdapter>(cascade_main_detector)), cascade_tracking_detector(
				cv::makePtr<cv::CascadeClassifier>(cascade_filename)), tracking_detector(
				cv::makePtr<HumanDetectorAdapter>(cascade_tracking_detector)), human_detector(
				main_detector, tracking_detector, parameters), canny_threshold(
				canny_edge_threshold), canny_kernel_size(
				canny_edge_kernel_size), canny_ratio(canny_edge_ratio), max_number_of_features(
				500), empty_flow_matrix(1, 1, CV_32F), flows_matrix(1, 1,
		CV_32F), max_value(0.0f), bg(cv::imread(bg_image_file)) {
	hog.setSVMDetector(cv::HOGDescriptor::getDefaultPeopleDetector());

	if (human_detector.run()) {
		std::cout << "Human detector is running" << std::endl;
	} else {
		std::cout << "Could not initialize human detector" << std::endl;
	}

	cv::GaussianBlur(bg, bg, cv::Size(kernel_size, kernel_size), 0.0);
	//cv::Mat temp;
	//cv::resize(bg, temp, cv::Size(200, 200));
	//bg = temp;
}

void VideoAnalyzer::set_bg_image(cv::Mat aFrame) {
	bg = aFrame;
}

void VideoAnalyzer::track_human(cv::Mat& frame, cv::Mat& result) {
	//Code taken from opecCV example - very slow - lets find another approach
	std::vector<cv::Rect> found, found_filtered;
	hog.detectMultiScale(result, found, 0, cv::Size(8, 8), cv::Size(32, 32),
			1.05, 2);

	filter_rects(found, found_filtered);

	for (size_t ff = 0; ff < found_filtered.size(); ++ff) {
		cv::Rect r = found_filtered[ff];
		cv::rectangle(frame, r.tl(), r.br(), cv::Scalar(0, 255, 0), 3);
	}

}

void VideoAnalyzer::detect_human(cv::Mat& frame, cv::Mat& gray_frame) {
	//std::cout << "Detecting human ... " << std::endl;
	std::vector<cv::Rect> body_candidates;
	human_detector.process(gray_frame);
	human_detector.getObjects(body_candidates);

	for (size_t i = 0; i < body_candidates.size(); i++) {
		cv::rectangle(frame, body_candidates[i], cv::Scalar(0, 255, 0)); /*Parameters taken from openCV example*/
	}

}

void VideoAnalyzer::backgroundSubstraction(cv::Mat& frame, cv::Mat& result) {
	//code based on the one found in https://bitbucket.org/ElissonMichael/tcc_implementacao/raw/8116c18f512da569fcc741bb23a5684c9ca582fd/Background/BackgroundSubtractionCam.py
	cv::Mat frame_smoothed;

	cv::GaussianBlur(frame, frame_smoothed, cv::Size(kernel_size, kernel_size),
			0.0);

	cv::absdiff(frame_smoothed, bg, result);

	cv::cvtColor(result, result, CV_BGR2GRAY);
	cv::threshold(result, result, threshold, 255, CV_THRESH_BINARY);

	cv::dilate(result, result, cv::Mat(), cv::Point(-1, -1), 3);
	cv::erode(result, result, cv::Mat(), cv::Point(-1, -1), 3);
}

//Check all this code as it was taken form openCV
void VideoAnalyzer::backgroundSubstractionMOG2(cv::Mat& frame,
		cv::Mat& result) {
	pMOG2->apply(frame, result);
}

void VideoAnalyzer::filter_rects(const std::vector<cv::Rect>& candidates,
		std::vector<cv::Rect>& objects) {
	size_t i, j;
	for (i = 0; i < candidates.size(); ++i) {
		cv::Rect r = candidates[i];

		for (j = 0; j < candidates.size(); ++j)
			if (j != i && (r & candidates[j]) == r)
				break;

		if (j == candidates.size())
			objects.push_back(r);
	}
}

/*Code based on openCV implementation of Canny edge detector*/
void VideoAnalyzer::detect_edges(cv::Mat&frame, cv::Mat& result) {
	cv::Mat edges;

	//take away noise in the image
	cv::blur(frame, edges, cv::Size(canny_kernel_size, canny_kernel_size));

	//Detect the edges
	cv::Canny(edges, edges, canny_threshold, canny_threshold * canny_ratio,
			canny_kernel_size);

	//copy edges:
	result = cv::Scalar::all(0); //create a black image then apply the mask
	frame.copyTo(result, edges);

}

void VideoAnalyzer::calucate_optical_flow(cv::Mat& prev_frame,
		cv::Mat& cur_frame) {
	std::vector<uchar> status; //based on openCV
	std::vector<float> err; //based on openCV
	cv::Mat flow_mat;
	float norm_val = 0.0f;

	cv::calcOpticalFlowFarneback(prev_frame, cur_frame, flow_mat, 0.5, 3, 15, 3,
			5, 1.2, 0);
	float value = (float) cv::norm(flow_mat);
	flows_matrix.push_back(value);
	if (value > max_value)
		max_value = value;
	if (value < 0.0)
		std::cout << "something went wrong" << std::endl;
}

void VideoAnalyzer::initialize_optical_flow_matrix() {
	flows_matrix = empty_flow_matrix;
}

float VideoAnalyzer::calculate_displacement(cv::Point2f& first_points,
		cv::Point2f &second_points) {
	return cv::norm(first_points - second_points);
}

bool wayToSort(int i, int j) {
	return i > j;
}

/*All code is based on openCV histogram example*/
void VideoAnalyzer::calculate_histogram(std::string file_name,
		cv::Mat &b_hist) {

	int histSize = 750;
	float range[] = { 0, 1500 };
	const float* histRange = { range };

	bool uniform = true;
	bool accumulate = false;
	int hist_w = 500;
	int hist_h = 500;
	int bin_w = cvRound((double) hist_w / histSize);
	cv::Mat histImage1(hist_h, hist_w, CV_8UC3, cv::Scalar(0, 0, 0));

	/*Create histogram of optical flows*/
	//cv::normalize(flows_matrix, flows_matrix, 0, 500, cv::NORM_MINMAX,
	//CV_32F);
	cv::calcHist(&flows_matrix, 1, 0, cv::Mat(), b_hist, 1, &histSize,
			&histRange, uniform, accumulate);

	/*Save histogram into a file*/
	cv::normalize(b_hist, b_hist, 0, histImage1.rows, cv::NORM_MINMAX, -1,
			cv::Mat());
	for (int i = 1; i < histSize; i++) {
		cv::line(histImage1,
				cv::Point(bin_w * (i - 1),
						hist_h - cvRound(b_hist.at<float>(i - 1))),
				cv::Point(bin_w * (i), hist_h - cvRound(b_hist.at<float>(i))),
				cv::Scalar(255, 0, 0), 2, 8, 0);
	}

	cv::imwrite(file_name, histImage1);

}
