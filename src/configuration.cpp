#include <configuration.hpp>
#include <iostream>
#include <string>
#include <fstream>
#include <opencv2/opencv.hpp>

Configuration::Configuration(std::string video_name) :
		cam_id(1), frames_per_second(30), width(800), height(600), codec(
				cv::VideoWriter::fourcc('X', 'V', 'I', 'D')), video_name(
				video_name), video_dir("./videos/single/"), bg_img_name(
				"bg.jpg"), bg_img_dir("./videos/bg/"), kernel_size(3), threshold(
				60), cascade_template_dir("./body_haar_cascade_templates/"), cascade_template_file(
				"haarcascade_lowerbody.xml"), canny_edge_kernel_size(3), canny_edge_ratio(
				3), canny_edge_threshold(50), training_dir(
				"./videos/training/vids/"), training_output_data(
				"./videos/training/data/"), training_output_hists(
				"./videos/training/data/img/"), training_dirs_file("dirs.txt"), training_img_files(
				"images.txt"), training_subdirs_file("sub_dirs.txt"), running_hist_file_name(
				"running_hists.png"), walking_hist_file_name(
				"walking_hists.png"), standing_hist_file_name(
				"standing_hists.png"), predict_dir("./videos/predict/vids/"), predict_output_data(
				"./videos/predict/data/"), predict_output_hists(
				"./videos/predict/data/img/"), predict_dirs_file("dirs.txt"), predict_img_files(
				"images.txt"), predict_subdirs_file("sub_dirs.txt") {

	std::string line;
	std::string::size_type sz;
	std::ifstream config("config.txt");

	if (config.is_open()) {
		std::cout << "Reding file: config.txt" << std::endl;
		int count = 0;
		while (getline(config, line)) {
			if (count == 0) {
				cam_id = std::stoi(line, &sz);
				std::cout << "Settiing CAM_ID to: " << cam_id << std::endl;
			} else if (count == 1) {
				frames_per_second = std::stoi(line, &sz);
				std::cout << "Settiing FRAMES_PER_SECOND to: "
						<< frames_per_second << std::endl;
			} else if (count == 2) {
				width = std::stoi(line, &sz);
				std::cout << "Settiing WIDTH to: " << width << std::endl;
			} else if (count == 3) {
				height = std::stoi(line, &sz);
				std::cout << "Settiing HEIGHT to: " << height << std::endl;
			} else if (count == 4) {
				video_dir = line;
				std::cout << "Setting video_dir to " << video_dir << std::endl;
			} else if (count == 5) {
				bg_img_dir = line;
				std::cout << "Setting bh_img_dir to " << bg_img_dir
						<< std::endl;
			} else if (count == 6) {
				bg_img_name = line;
				std::cout << "Setting bg_img_name to " << bg_img_name
						<< std::endl;
			} else if (count == 7) {
				kernel_size = std::stoi(line, &sz);
				std::cout << "Setting kernel_size to " << kernel_size
						<< std::endl;
			} else if (count == 8) {
				threshold = std::stoi(line, &sz);
				std::cout << "Setting threshold to " << threshold << std::endl;
			} else if (count == 9) {
				cascade_template_dir = line;
				std::cout << "Setting cascade_template_dir to: "
						<< cascade_template_dir << std::endl;
			} else if (count == 10) {
				cascade_template_file = line;
				std::cout << "Setting cascade_template_file to: "
						<< cascade_template_dir << std::endl;
			} else if (count == 11) {
				canny_edge_kernel_size = std::stoi(line, &sz);
				std::cout << "Setting canny edge kerner size to: "
						<< canny_edge_kernel_size << std::endl;
			} else if (count == 12) {
				canny_edge_ratio = std::stoi(line, &sz);
				std::cout << "Setting canny edge ratio to: " << canny_edge_ratio
						<< std::endl;

			} else if (count == 13) {
				canny_edge_threshold = std::stoi(line, &sz);
				std::cout << "Setting canny_edge_threshold to: "
						<< canny_edge_threshold << std::endl;
			} else if (count == 14) {
				training_dir = line;
				std::cout << "Setting training_dir to: " << training_dir
						<< std::endl;
			} else if (count == 15) {
				training_output_data = line;
				std::cout << "Setting training_output_data to: "
						<< training_output_data << std::endl;
			} else if (count == 16) {
				training_output_hists = line;
				std::cout << "Setting training_output_hists to: "
						<< training_output_hists << std::endl;
			} else if (count == 17) {
				training_dirs_file = line;
				std::cout << "Setting training_dirs_file to: "
						<< training_dirs_file << std::endl;
			} else if (count == 18) {
				training_img_files = line;
				std::cout << "Setting training_img_files to: "
						<< training_img_files << std::endl;
			} else if (count == 19) {
				training_subdirs_file = line;
				std::cout << "Setting training_subdirs_file to: "
						<< training_subdirs_file << std::endl;
			} else if (count == 20) {
				predict_dir = line;
				std::cout << "Setting predict dir to: " << predict_dir
						<< std::endl;
			}
			count++;
		}

		config.close();
	} else {
		std::cout << "counter.txt not found, using default values" << std::endl;
	}

	std::ifstream counter("counter.txt");

	if (counter.is_open()) {
		std::cout << "Reding file: counter.txt" << std::endl;
		while (getline(counter, line)) {
			video_counter = std::stoi(line, &sz);
			std::cout << "Settiing vid_counter to: " << video_counter
					<< std::endl;
		}

		counter.close();
	} else {
		std::cout << "config.txt not found, setting counter to 100"
				<< std::endl;
		video_counter = 100;
	}
}

std::string Configuration::get_video_name() {
	return video_name;
}
std::string Configuration::get_video_dir() {
	return video_dir;
}
std::string Configuration::get_video_full_name() {
	return video_dir + video_name;
}

std::string Configuration::get_bg_img_name() {
	return bg_img_name;
}
std::string Configuration::get_bg_img_dir() {
	return bg_img_dir;
}
std::string Configuration::get_bg_img_full_name() {
	return bg_img_dir + bg_img_name;
}

int Configuration::get_cam_id() {
	return cam_id;
}

int Configuration::get_codec() {
	return codec;
}

int Configuration::get_kernel_size() {
	return kernel_size;
}

int Configuration::get_threshold() {
	return threshold;
}

int Configuration::get_frames_per_second() {
	return frames_per_second;
}
int Configuration::get_width() {
	return width;
}

int Configuration::get_height() {
	return height;
}

std::string Configuration::get_cascade_template_file() {
	return cascade_template_file;
}

std::string Configuration::get_cascade_templates_dir() {
	return cascade_template_dir;
}

std::string Configuration::get_cascade_template_full_name() {
	return cascade_template_dir + cascade_template_file;
}

int Configuration::get_video_counter() {
	return video_counter;
}
void Configuration::update_counter_file(int val) {
	std::ofstream counter_file;
	counter_file.open("counter.txt");
	counter_file << val;
	counter_file.close();
}
int Configuration::get_canny_edge_kernel() {
	return canny_edge_kernel_size;
}
int Configuration::get_canny_edge_ratio() {
	return canny_edge_ratio;
}
int Configuration::get_canny_edge_threshold() {
	return canny_edge_threshold;
}
std::string Configuration::get_training_dir() {
	return training_dir;
}
std::string Configuration::get_training_output_data() {
	return training_output_data;
}
std::string Configuration::get_training_output_hists() {
	return training_output_hists;
}
std::string Configuration::get_training_dirs_files() {
	return training_dirs_file;
}
std::string Configuration::get_training_img_files() {
	return training_img_files;
}
std::string Configuration::get_training_subdirs_file() {
	return training_subdirs_file;
}

std::string Configuration::get_running_hist_file_name() {
	return running_hist_file_name;
}
std::string Configuration::get_walking_hist_file_name() {
	return walking_hist_file_name;
}
std::string Configuration::get_standing_hist_file_name() {
	return standing_hist_file_name;
}

std::string Configuration::get_predict_dir() {
	return predict_dir;
}
std::string Configuration::get_predict_output_data() {
	return predict_output_data;
}
std::string Configuration::get_predict_output_hists() {
	return predict_output_hists;
}
std::string Configuration::get_predict_dirs_files() {
	return predict_dirs_file;
}
std::string Configuration::get_predict_img_files() {
	return predict_img_files;
}
std::string Configuration::get_predict_subdirs_file() {
	return predict_subdirs_file;
}

