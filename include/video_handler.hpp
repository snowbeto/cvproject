#ifndef VIDEO_HANDLER_HPP
#define VIDEO_HANDLER_HPP

#include <iostream>
#include <configuration.hpp>
#include <video_analyzer.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/video.hpp"
#include <opencv2/optflow.hpp>
#include <opencv2/ml.hpp>
#include <thread>
#include <vector>
#include <chrono>
#include <time.h>
#include <cmath>
#include <vector>

class VideoHandler {
private:

	Configuration config_object;
	VideoAnalyzer vid_analyzer;
	cv::VideoCapture vidCap;
	cv::VideoWriter vid_writer;

	cv::Mat frame;
	int codec;
	bool GRAB;
	bool RECORD;
	bool RUN;

	void create_video_writter();
	void release_video_witter();
	std::vector<cv::Mat> video_buffer;
	void gui_controllers(int &keyPressed);

	int frame_counter;
	int counter;

	bool initialyze_opt_flow;
	void calculate_fps();

	void rotate_frame(cv::Mat &aFrame);

	void take_bg_image();

	/*Calculate fps*/
	time_t timer;
	struct tm y2k;
	double seconds_s, seconds, total_s;
	double current_secs;
	int fps;

	//trainning
	cv::Mat walking_hists, running_hists, standing_hists;
	bool initialize_walking_hists_matrix;
	bool initialize_running_hists_matrix;
	bool initialize_standing_hists_matrix;
	bool can_predict;
	void train_svn();
	cv::Ptr<cv::ml::SVM> svm_running_walking;
	cv::Ptr<cv::ml::SVM> svm_walking_standing;
	cv::Mat running_hists_mean, walking_hists_mean, standing_hists_mean;

	float standing_norm_value;
	float walking_norm_value;
	float running_float_value;

	bool record_and_predict;
	bool rotate_frames;

public:
	//VideoHandler(std::string video_name);
	VideoHandler();
	void run();
	void record();
	void train_images();
	void train_videos();
	void predict();
	void save_video();
	void predict(std::string fileName);

};

#endif
