#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP

#include <string>

class Configuration {
public:
	Configuration(std::string video_name);

	std::string get_video_name();
	std::string get_video_dir();
	std::string get_video_full_name();

	std::string get_bg_img_name();
	std::string get_bg_img_dir();
	std::string get_bg_img_full_name();

	int get_frames_per_second();
	int get_width();
	int get_height();

	int get_cam_id();
	int get_codec();

	int get_kernel_size();
	int get_threshold();

	int get_video_counter();
	void update_counter_file(int val);
	std::string get_cascade_template_file();
	std::string get_cascade_templates_dir();
	std::string get_cascade_template_full_name();

	int get_canny_edge_kernel();
	int get_canny_edge_ratio();
	int get_canny_edge_threshold();

	std::string get_training_dir();
	std::string get_training_output_data();
	std::string get_training_output_hists();
	std::string get_training_dirs_files();
	std::string get_training_img_files();
	std::string get_training_subdirs_file();
	std::string get_running_hist_file_name();
	std::string get_walking_hist_file_name();
	std::string get_standing_hist_file_name();
	std::string get_predict_dir();
	std::string get_predict_output_data();
	std::string get_predict_output_hists();
	std::string get_predict_dirs_files();
	std::string get_predict_img_files();
	std::string get_predict_subdirs_file();

private:
	int cam_id;
	int frames_per_second;
	int width;
	int height;
	int codec;
	int kernel_size;
	int threshold;
	int video_counter;
	std::string video_name;
	std::string video_dir;
	std::string bg_img_name;
	std::string bg_img_dir;
	std::string cascade_template_dir;
	std::string cascade_template_file;
	std::string running_hist_file_name;
	std::string walking_hist_file_name;
	std::string standing_hist_file_name;
	int canny_edge_threshold;
	int canny_edge_kernel_size;
	int canny_edge_ratio;

	std::string training_dir;
	std::string training_output_data;
	std::string training_output_hists;
	std::string training_dirs_file;
	std::string training_img_files;
	std::string training_subdirs_file;

	std::string predict_dir;
	std::string predict_output_data;
	std::string predict_output_hists;
	std::string predict_dirs_file;
	std::string predict_img_files;
	std::string predict_subdirs_file;

};

#endif
