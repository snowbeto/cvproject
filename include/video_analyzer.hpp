#ifndef VIDEO_ANALYZER_HPP
#define VIDEO_ANALYZER_HPP

#include <iostream>
#include <configuration.hpp>
#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/optflow.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/video.hpp"
#include <human_detector.hpp>
#include <string>

class VideoAnalyzer {
public:
	VideoAnalyzer(int kernel_size, int threshold, std::string bg_image_file,
			std::string cascade_template_file, int canny_edge_kernel_size,
			int canny_edge_ratio, int canny_edge_threshold);
	void backgroundSubstraction(cv::Mat& frame, cv::Mat& result);
	void backgroundSubstractionMOG2(cv::Mat& frame, cv::Mat& result);

	void track_human(cv::Mat& frame, cv::Mat& result);
	void detect_human(cv::Mat& frame, cv::Mat& gray_frame);
	void detect_edges(cv::Mat&frame, cv::Mat& result);
	void initialize_optical_flow_matrix();
	void calucate_optical_flow(cv::Mat& prev_frame, cv::Mat& cur_frame);
	float calculate_displacement(cv::Point2f& first_points,
			cv::Point2f &second_point);
	void calculate_histogram(std::string file_name, cv::Mat &b_hist);
	void set_bg_image(cv::Mat aFrame);

private:
	void filter_rects(const std::vector<cv::Rect>& candidates,
			std::vector<cv::Rect>& objects);

	cv::Ptr<cv::BackgroundSubtractor> pMOG2; //code taken from opencv

	int kernel_size;
	int threshold;

	cv::Mat bg;
	//set HOG Descriptor
	cv::HOGDescriptor hog;
	std::string bg_image_file;

	/*Code based on openCsV example*/
	std::string cascade_filename;
	cv::Ptr<cv::CascadeClassifier> cascade_main_detector;
	cv::Ptr<cv::CascadeClassifier> cascade_tracking_detector;
	cv::Ptr<cv::DetectionBasedTracker::IDetector> main_detector;
	cv::Ptr<cv::DetectionBasedTracker::IDetector> tracking_detector;
	cv::DetectionBasedTracker::Parameters parameters;
	cv::DetectionBasedTracker human_detector;
	/*Code based on opedCV canny edge detection*/
	int canny_threshold;
	int canny_ratio;
	int canny_kernel_size;

	//optical flow parameters
	std::vector<cv::Point2f> prev_features, next_features;
	int max_number_of_features;
	cv::Mat empty_flow_matrix;
	cv::Mat flows_matrix;
	std::vector<float> flows;
	float max_value;

};

#endif
